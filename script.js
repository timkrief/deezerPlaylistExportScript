let songs = [];
let count = 0;
let total = document.createElement("div");
let score = document.createTextNode("0");
total.appendChild(score);
total.style = "position: fixed; z-index: 200; border-radius: 1em; color: white; background-color: rgba(0, 0, 0, 0.8); top: 1em; right: 1em; padding: 1em; font-size: 2em"


let catchSongs = setInterval(function(){
  let DOMsongs = document.getElementsByClassName("song")

  for (var i = 0; i < DOMsongs.length; i++) {
    let index = DOMsongs[i].getElementsByClassName("datagrid-track-number")[0].textContent - 1;
    if(!songs[index]){
      count++;
      songs[index] = {
        title: DOMsongs[i].getElementsByClassName("cell-title")[0].textContent,
        artist: DOMsongs[i].getElementsByClassName("cell-artist")[0].textContent,
        album: DOMsongs[i].getElementsByClassName("cell-album")[0].textContent
      }
    }
  }
  score.nodeValue = count.toString();
},300)

total.appendChild(document.createElement("br"))
let stop = document.createElement("button");
stop.appendChild(document.createTextNode("Get JSON!"))
stop.addEventListener("click", function(){
  clearTimeout(catchSongs);
  document.body.innerHTML = JSON.stringify({songs}, null, 2);
})
total.appendChild(stop)
document.body.appendChild(total);
