
# Deezer Playlist Export Script

A script to export deezer playlist information as a JSON file. 

## Information

I needed to export my playlists from deezer but I found no options to do so. 
That's why I made this script rapidly. Maybe you need to export playlist 
information too, that's why I made the code available, enjoy.

## How to use the script

Just execute the script on a deezer playlist page of your choice, then scroll to 
display all the song names* , and then click on the button top right of your 
screen, it will display the data as a JSON file.

*deezer does not load the song information for song that are not displayed, that 
is why you have to scroll through the entire page.